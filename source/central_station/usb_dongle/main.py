import sys
import json
import random
import bluetooth
from ble_central import BLECentralManagement
import _thread

_debug = 0


def print_debug(level, *var_args):
	if _debug >= level:
		print(*var_args)


class driveBluetoothCentral:

	def __init__(self, com):
		# :param com: instance of DriveCentral object (so we can use its send_to_com method)
		ble_for_central = bluetooth.BLE()
		self.message_received = None
		self.com = com
		self.central = BLECentralManagement(ble_for_central)
		self.peripheral_state = None
		_thread.start_new_thread(self.run_ble_thread, ())

	def receive_message_part(self, data):
		# manage messages received from BLE
		print_debug(2, "On RX: ", data.decode('utf-8'))
		if self.message_received is None:
			self.message_received = data.decode('utf-8')
		else:
			self.message_received += data.decode('utf-8')
		if self.message_received.find("<END>") > 0:
			return self.message_received[:-5]
		else:
			return None

	def send_to_peripheral(self, msg):
		# message received from COM port to send to peripheral
		self.central.write(msg)

	def connect(self, addr_type, addr, name, state):
		if addr_type is not None and state is not None:
			mac_address = ':'.join(['{:02x}'.format(addr[ele]) for ele in range(0, 6)])
			print_debug(1, "Found peripheral:>", mac_address)
			if self.central.connect():
				self.com.send_to_com({"type": "connected", "state": state, "addr": mac_address, 'name': name})
				self.peripheral_state = state
			else:
				self.com.send_to_com({"type": "connectFailed", "addr": mac_address})
		else:
			self.com.send_to_com({"type": "noPeripheralFound"})

	def run_ble_thread(self):
		while True:
			# wait an event is received over BLE
			self.central.event_flag.acquire()
			print_debug(2, 'central event', self.central.event_type)
			if self.central.event_type == 'disconnected':
				# disconnected event received from BLE, send information to COM port
				self.peripheral_state = None
				self.com.send_to_com({"type": "disconnected"})
			elif self.central.event_type == 'mtu_exchanged':
				self.com.send_to_com({"type": "mtuExchanged"})
			elif self.central.event_type == 'notify':
				# message received from BLE. If it is complete, send it to COM port
				complete_message = self.receive_message_part(self.central.event_data)
				self.message_received = None
				self.central.event_data = None
				if complete_message:
					# send this message to COM port
					msg_dict = {"type": "fromPeripheral", "msg": complete_message}
					self.com.send_to_com(msg_dict)
			elif self.central.event_type == 'write_done':
				self.com.send_to_com({'type': 'writeDone'})
			elif self.central.event_type == 'scan_done':
				# "scan_done" event received from BLE, try to connect if a peripheral was found
				self.connect(*self.central.event_data)
				self.central.event_data = None
			self.central.event_type = None


class DriveCentral:
	
	def __init__(self):
		self.ble = driveBluetoothCentral(self)
		self.dongle_msg_id = 0
		self.callback_for_COM_msg_type = {
			"scanAndConnect": self.ble_scan_and_connect, "disconnect": self.ble_disconnect,
			"toPeripheral": self.send_to_peripheral}

	def add_callback_for_com_msg_type(self, msg_type, callback):
		if msg_type in self.callback_for_COM_msg_type:
			print('replace previous callback for msg type', msg_type)
		self.callback_for_COM_msg_type[msg_type] = callback

	def send_to_com(self, msg_dict):
		msg_dict['dongleId'] = self.dongle_msg_id
		print(json.dumps(msg_dict))
		self.dongle_msg_id += 1

	def send_to_peripheral(self, msg):
		# message received from COM port to send to peripheral
		self.ble.send_to_peripheral(msg)

	def ble_scan_and_connect(self, recipient, expect_peripheral_name=None):
		# start a BLE scan; when it is complete we will see to connect on peripheral found
		self.ble.central.scan(recipient=recipient, expect_peripheral_name=expect_peripheral_name)

	def ble_disconnect(self):
		# request to disconnect from BLE peripheral
		self.ble.central.disconnect()

	def run(self):
		# manage messages received from PC through UART
		print("dongle start")
		while 1:
			try:
				line = input()
			except KeyboardInterrupt:
				# when ctrl-C is sent to dongle, we receive a KeyboardInterrupt
				sys.exit(0)
			if not len(line) or line[0] != "{":
				continue
			received_msg_dict = json.loads(line)
			if 'type' not in received_msg_dict:
				print("no type found")
				continue
			msg_type = received_msg_dict['type']
			if msg_type not in self.callback_for_COM_msg_type:
				print("unknown message type", received_msg_dict)
				print('known types are', list(self.callback_for_COM_msg_type))
				continue
			del(received_msg_dict['type'])
			del(received_msg_dict['pcId'])
			answer_msg_dict = self.callback_for_COM_msg_type[msg_type](**received_msg_dict)
			if answer_msg_dict:
				self.send_to_com(answer_msg_dict)


# -------------------------------
#             MAIN
# -------------------------------
def randStr(length=10):
	"""Generate a random string of characters
	:param length: number of characters"""
	allowedChar = "abcdefghijklmnopqrstuvwxyz0123456789"
	out = ''
	for _ in range(length):
		out += allowedChar[random.randrange(len(allowedChar))]
	return out


def test_msg_from_central_to_PC(msg):
	return {"type": "testCentralToPC", "received": msg, "answer": "hello PC, I'm central"}


def test_send_pc2dongle(**kvDict):
	numKey,	maxLenKey, maxLenVal = 0, 0, 0
	if type(kvDict) is not dict:
		print('bad message content', type(kvDict))
		return
	for key in kvDict:
		numKey += 1
		lenKey, lenVal = len(key), len(kvDict[key])
		maxLenKey = max(lenKey, maxLenKey)
		maxLenVal = max(lenVal, maxLenVal)
	return {'type': 'testSendAnswer', 'num_key': numKey, 'max_len_key': maxLenKey, 'max_len_val': maxLenVal}


def test_send_dongle2pc(num_key=5, len_key=5, len_val=10):
	# Generate an answer with very variable length to check message is then correctly transferred from dongle to PC
	answer = {'type': 'testRecAnswer', 'num_key': num_key, 'len_key': len_key, 'len_val': len_val}
	for _ in range(num_key):
		answer[randStr(len_key)] = randStr(len_val)
	return answer


drv = DriveCentral()
drv.add_callback_for_com_msg_type("testPcToCentral", test_msg_from_central_to_PC)
drv.add_callback_for_com_msg_type('testSizeSend', test_send_pc2dongle)
drv.add_callback_for_com_msg_type('testSizeRec', test_send_dongle2pc)
drv.run()
