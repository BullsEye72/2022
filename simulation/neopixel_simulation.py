import tkinter as tk

SCREEN_CAPTURE = False # /!\ Do not forget to create the eps directory first :-)

class NeoPixel:
    # R G B
    ORDER = (0, 1, 2)

    def __init__(self, pin, n, bpp=3, timing=1):
        self.pin = pin
        self.n = n

        if n != 4:
            print("ERROR: ONLY FOUR LEDS IN THIS SIMULATION")
            exit(1)

        self.bpp = bpp
        self.buf = bytearray(n * bpp)

        # Tkinter window setup, as big as the nice background image
        self.root = tk.Tk()
        # Important: for an unknown reason, the background image must be loaded
        # before creating the Canvas() but after the tk.Tk() constructor...
        self.bgimg = tk.PhotoImage(file="alphabot2-ar-back-view.gif")
        self.canvas = tk.Canvas(self.root, width=self.bgimg.width(), height=self.bgimg.height())
        self.root.title("Alphabot2-ar leds simulation - This Is Super Cool :-)")
        self.canvas.pack(side='top', fill='both', expand='yes')
        self.canvas.create_image(0, 0, image=self.bgimg, anchor='nw')

        # Get window size
        win_w = int(self.canvas["width"])
        win_h = int(self.canvas["height"])


        # Prepare and draw squares, only the fill color will be changed later
        self.squares = []
        w = 28
        h = w
        # outline="" to remove the black thin border

        # LED1 from the back
        self.squares.append(self.canvas.create_rectangle(668, 460, 668 + w, 460 + h, fill="black"))
        # LED2 from the back
        self.squares.append(self.canvas.create_rectangle(576, 467, 576 + w, 467 + h, fill="black"))
        # LED3 from the back
        self.squares.append(self.canvas.create_rectangle(379, 464, 379 + w, 464 + h, fill="black"))
        # LED4 from the back
        self.squares.append(self.canvas.create_rectangle(294, 456, 294 + w, 456 + h, fill="black"))

        if SCREEN_CAPTURE:
            self.counter = 0


    def __len__(self):
        return self.n

    def __setitem__(self, i, v):
        offset = i * self.bpp
        for i in range(self.bpp):
            self.buf[offset + self.ORDER[i]] = v[i]

    def __getitem__(self, i):
        offset = i * self.bpp
        return tuple(self.buf[offset + self.ORDER[i]] for i in range(self.bpp))

    def fill(self, v):
        b = self.buf
        for i in range(self.bpp):
            c = v[i]
            for j in range(self.ORDER[i], len(self.buf), self.bpp):
                b[j] = c

    def rgb2hexa(self, r, g, b):
        return '#%02x%02x%02x' % (r, g, b)

    def write(self):
        buf = self.buf
        squares = self.squares
        canvas = self.canvas
        index = 0
        for i in range(self.n):
            canvas.itemconfigure(squares[i], fill=self.rgb2hexa(buf[index], buf[index + 1], buf[index + 2]))
            index += 3
        self.canvas.update()

        if SCREEN_CAPTURE:
            self.counter += 1
            filename = "eps/snapshot_" + "{0:04d}.eps".format(self.counter)
            print(filename)
            self.canvas.postscript(file=filename, colormode='color')

# Unitary tests
# $> python3 neopixel_simulation.py
if __name__ == "__main__":

    print("coucou")
    import neopixel_simulation as neopixel

    class FoursNeoPixel():
        def __init__(self, pin_number):
            self._pin = pin_number
            self._max_leds = 4
            self._leds = neopixel.NeoPixel(self._pin, 4)

        def set_led(self, addr, red, green, blue):
            if addr >= 0 and addr < self._max_leds:
                self._leds[addr] = (red, green, blue)

        def show(self):
            self._leds.write()

        def clear(self):
            for i in range(0, self._max_leds):
                self.set_led(i, 0, 0, 0)
            self.show()

    leds = FoursNeoPixel(0)

    leds.set_led(0, 255, 0, 0)      # red
    leds.show()
    text = input("Enter to continue")

    leds.set_led(1, 0, 255, 0)      # green
    leds.show()
    text = input("Enter to continue")

    leds.set_led(2, 0, 0, 255)      # blue
    leds.show()
    text = input("Enter to continue")

    leds.set_led(3, 255, 255, 255)  # white
    leds.show()
    text = input("Enter to continue")

    leds.clear()
    text = input("Enter to continue")

    print("Bye :-)")
