All these Python files work on both your favorite console and the AlphaBot2.


## Dependencies
* For the "neopixel" leds, we use tkinter:
``` bash
# on Ubuntu-like
sudo apt-get install python3-tk

# or
pip install tk
```

* For the buzzer, we use [Sound eXchange (SoX)](http://sox.sourceforge.net/) for **Linux** and **macOS**. For **Windows**, sox works fine but there is an easier alternative without dependencies called [winsound](https://docs.python.org/3/library/winsound.html):
``` bash
### Linux
sudo apt install sox
play -q -n -V0 synth 1 sin 440 # simple test command to check your audio


### macOS
# Install Homebrew (https://brew.sh/)
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
# Install sox with Homebrew (https://formulae.brew.sh/formula/sox)
brew install sox
play -q -n -V0 synth 1 sin 440 # simple test command to check your audio


### Windows
# Without sox (preferred): (probably not required in most Python3 installation :-)
pip install winsound

# With sox: install sox from http://sox.sourceforge.net/
# then do not forget to "copy sox.exe play.exe" and to update your PATH env
play -q -n -V0 -t waveaudio synth 1 sin 440 # simple test command to check your audio, -t waveaudio is required

```

## To run them in simulation mode...
Simply enter:
``` bash
# For the 4 leds
./main_alphabot2_leds.py

# For the buzzer
./main_alphabot2_buzzer.py
```

## Leds snapshots
In the **"neopixel_simulation.py"**, it is possible to activate the **snapshot feature** to dump all new led updates.
``` Python
...
SCREEN_CAPTURE = False # /!\ Do not forget to create the eps directory first :-)
...
```
The snapshots are generated inside the "eps" directory in the "eps" format.

### Convert tk eps snapshot to video with inkscape & ffmpeg
under construction

### Convert tk eps to animated gif with inkscape & imagemagick & gifsicle
``` bash
# from the eps directory
export output=leds.gif
export inkscape_bin=~/Desktop/InkScape
export width=1000
export fps=1
# Convert eps to png with inkscape
for n in `ls *.eps | cut -f1 -d'.'`; do ${inkscape_bin} $n.eps -o $n.png -y 255 -w $width; done
# Convert and merge png to an animated gif
convert -loop 0 -delay 1x$fps -layers Optimize *.png ${output}
# Optimize the animated gif with gifsicle
gifsicle -O3 ${output} -o ${output}
```

### Convert tk eps to animated svg with inkscape & svgasm & svgcleaner
under construction

