import sys

# Detect if we are in simulation mode
if sys.implementation.name == 'cpython':
    SIMULATION = True
else:
    SIMULATION = False

if SIMULATION:
    import utime_simulation as utime

    if sys.platform.startswith('linux') or sys.platform.startswith('darwin'):
        # Linux & macOS: sox
        import os
        def playsound(frequency_hz, duration_ms):
            os.system("play -q -n -V0 synth {} sin {}".format(str(duration_ms / 1000.0), str(frequency_hz)))

    elif sys.platform.startswith('win32'):
        # Windows: winsound is preferred, else sox
        try:
            import winsound
        except ImportError:
            # sox is used, please check your path to "play" if no sound
            import os
            def playsound(frequency_hz, duration_ms):
                os.system("play -q -n -V0 -t waveaudio synth {} sin {}".format(str(duration_ms / 1000.0), str(frequency_hz)))

        else:
            # winsound is used (preferred)
            def playsound(frequency_hz, duration_ms):
                winsound.Beep(frequency_hz, duration_ms)

    else:
        print("ERROR: platform not recognized, bye...")
        exit(1)

else:
    import utime




class Buzzer:
    def __init__(self):
        pass

    if not SIMULATION:
        def _pitch(self, robot, noteFrequency, noteDuration, silence_ms = 10):
            # ------------------------------------
            # Vittascience
            # Example for playing sound
            # ------------------------------------
            # Please write your code below ;-)
            pass

    else:
        # SIMULATION part
        def _pitch_simu(self, robot, noteFrequency, noteDuration, silence_ms = 10):
            if noteFrequency is not 0:
                playsound(noteFrequency, noteDuration)
            else:
                utime.sleep_ms(int(noteDuration))
            utime.sleep_ms(silence_ms)

    def pitch(self, robot, noteFrequency, noteDuration, silence_ms = 10):
        #print("[DEBUG][pitch]: Frequency {:5} Hz, Duration {:4} ms, silence {:4} ms".format(noteFrequency, noteDuration, silence_ms))
        if SIMULATION:
            self._pitch_simu(robot, noteFrequency, noteDuration, silence_ms)
        else:
            self._pitch(robot, noteFrequency, noteDuration, silence_ms)
