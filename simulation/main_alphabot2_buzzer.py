#!/usr/bin/env python3

import sys

# Detect if we are in simulation mode
if sys.implementation.name == 'cpython':
    print("SIMULATION mode")
    SIMULATION = True
else:
    SIMULATION = False


if SIMULATION:
    from stm32_alphabot_v2_simulation import AlphaBot_v2
    import utime_simulation as utime
else:
    from stm32_alphabot_v2 import AlphaBot_v2
    import utime

import buzzer
buz = buzzer.Buzzer()

# Carribean Pirates
def BuzzerCarribeanPirates(robot):
    SW_NOTES_1 = [330, 392, 440, 440, 0, 440, 494, 523, 523, 0, 523, 587, 494, 494, 0, 440, 392, 440, 0]
    SW_DURATION_1 = [125, 125, 250, 125, 125, 125, 125, 250, 125, 125, 125, 125, 250, 125, 125, 125, 125, 375, 125]
    SW_NOTES_2 = [330, 392, 440, 440, 0, 440, 523, 587, 587, 0, 587, 659, 698, 698, 0, 659, 587, 659, 440, 0, 440, 494, 523, 523, 0, 587, 659, 440, 0, 440, 523, 494, 494, 0, 523, 440, 494, 0]
    SW_DURATION_2 = [125, 125, 250, 125, 125, 125, 125, 250, 125, 125, 125, 125, 250, 125, 125, 125, 125, 125, 250, 125, 125, 125, 250, 125, 125, 250, 125, 250, 125, 125, 125, 250, 125, 125, 125, 125, 375, 375]
    for j in range(2):
        for i in range(len(SW_NOTES_1)):
            buz.pitch(robot, SW_NOTES_1[i], SW_DURATION_1[i])
    for k in range(len(SW_NOTES_2)):
        buz.pitch(robot, SW_NOTES_2[k], SW_DURATION_2[k])

# Gamme
def BuzzerGamme(robot):
    G_NOTES = [261.63, 293.66, 329.54, 349.23, 392, 440, 493.88, 523.25]
    for i in range(len(G_NOTES)):
        buz.pitch(robot, G_NOTES[i], 250, 50)

# Starwars
def BuzzerStarWars(robot):
    SW_NOTES = [293.66, 293.66, 293.66, 392.0, 622.25, 554.37, 523.25, 454, 932.32, 622.25, 554.37, 523.25, 454, 932.32, 622.25, 554.37, 523.25, 554.37, 454]
    SW_DURATION = [180, 180, 180, 800, 800, 180, 180, 180, 800, 400, 180, 180, 180, 800, 400, 180, 180, 180, 1000]
    SW_SLEEP = [40, 40, 40, 100, 100, 40, 40, 40, 100, 50, 40, 40, 40, 100, 50, 40, 40, 40, 100]
    for i in range(len(SW_NOTES)):
        buz.pitch(robot, SW_NOTES[i], SW_DURATION[i], SW_SLEEP[i])

# R2D2
def BuzzerR2D2(robot):
    R2D2_NOTES = [3520, 3135.96, 2637.02, 2093, 2349.32, 3951.07, 2793.83, 4186.01, 3520, 3135.96, 2637.02, 2093, 2349.32, 3951.07, 2793.83, 4186.01]
    for i in range(len(R2D2_NOTES)):
        buz.pitch(robot, R2D2_NOTES[i], 80, 20)

# -------------------------------
# MAIN
# -------------------------------
alphabot = AlphaBot_v2()
utime.sleep(1)

# ------ Bip -------
# Buzzer high: 0
alphabot.controlBuzzer(0)
utime.sleep_us(2000)
# buzzer low: 1
alphabot.controlBuzzer(1)
utime.sleep(5)

# --------Vittascience exemples --------
print('Vittascience: CarribeanPirates')
BuzzerCarribeanPirates(alphabot)
utime.sleep(2)
print('Vittascience: StarWars')
BuzzerStarWars(alphabot)