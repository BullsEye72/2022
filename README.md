# Ceci est le dépôt du sujet des 24h du code 2022 proposé par ST Le Mans.

Repertoires desciption:
* Documentation: documents à lire absolument.
* Firmware: si besoin de reflasher vos USB dongle, robot... Ils sont déjà flashés.
* simulation: environement de developement sur PC qui simule certaines fonctions HW du robot. Cela permet de travailler sur machine Host le plus longtemps possible avant test crash sur target.
* source: libraire provenant de vittascience ou de ST qui permet d'accéder aux différentes fonctions HW du robot. Bien lire la présentation d'archtitecture associé (Page 4).



